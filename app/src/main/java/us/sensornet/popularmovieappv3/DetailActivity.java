package us.sensornet.popularmovieappv3;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Movie;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import us.sensornet.popularmovieappv3.Api.Service;
import us.sensornet.popularmovieappv3.Api.ServiceInterface;
import us.sensornet.popularmovieappv3.Helper.GenreHelper;
import us.sensornet.popularmovieappv3.Helper.ImageHelper;
import us.sensornet.popularmovieappv3.Model.MovieListModel;
import us.sensornet.popularmovieappv3.Model.MovieModel;
import us.sensornet.popularmovieappv3.Model.MovieReviewListModel;
import us.sensornet.popularmovieappv3.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3.Model.MovieTrailerListModel;
import us.sensornet.popularmovieappv3.Model.MovieTrailerModel;
import us.sensornet.popularmovieappv3.YouTube.YoutubeImageHelper;
import us.sensornet.popularmovieappv3.YouTube.YoutubeImageInterface;

public class DetailActivity extends AppCompatActivity implements ServiceInterface, YoutubeImageInterface {
    @SuppressWarnings("unused")
    private final String TAG = getClass().getSimpleName();

    @SuppressWarnings("FieldCanBeLocal")
    private MovieModel mMovie;
    private List<MovieTrailerModel> mMovieTrailers;

    private Service mApiService;
    private String mApiKey = BuildConfig.theMovieDB3_Api_Key;

    private LayoutInflater mInflater;
    private LinearLayout mTrailerLayout;
    private int mTrailerCount;

    private String mTrailerUrl = "https://www.youtube.com/watch?v=%s";

    private List<MovieReviewModel> mReviews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Service movieService = new Service(this, false);

        // Get the layout and inflater
        // Get an inflater from movie trailer list
        mTrailerLayout = findViewById(R.id.ll_movie_trailer_list);
        mInflater = LayoutInflater.from(this);
        mTrailerCount = 0;

        Gson gson = new Gson();
        String strJson = getIntent().getStringExtra("jsonMovie");
        mMovie = gson.fromJson(strJson, MovieModel.class);
        Objects.requireNonNull(mMovie, "mMovie not initialized in onCreate");

        // Get Movie Trailers
        mApiService = new Service(this, true);
        // Add this activity as listener
        mApiService.addListener(this);
        // Call the apiService
        mApiService.movieTtrailerRequest(String.valueOf(mMovie.getId()));

        // Get Movie Reviews
        // Moved this call to OnMovieTrailerDataReady to serialize the calls.
        // Should also include it in OnMovieTrailerDataFailed.
        //mApiService.movieReviewsRequest(String.valueOf(mMovie.getId()));

        showMovieDetails(mMovie);

    }


    // Movie Details
    private void showMovieDetails(@NonNull MovieModel movie) {
        ImageView imageView = findViewById(R.id.img_poster);
        TextView tvTitle = findViewById(R.id.tv_title);
        TextView tvReleaseDate = findViewById(R.id.tv_release_date);
        TextView tvAverageVotes = findViewById(R.id.tv_average_vote);
        TextView tvSynopsis = findViewById(R.id.tv_synopsis);
        TextView tvGenres = findViewById(R.id.tv_genre);

        String Url = ImageHelper.ImageWidth.w500 + "/" + movie.getPosterPath();
        imageView.setTag(movie.getId());
        ImageHelper.getDrawableFromNetwork(imageView, Url);

        GenreHelper genreHelper = new GenreHelper();

        // Genres
        StringBuilder strGenres = new StringBuilder();
        List<Integer> genres = movie.getGenreIds();
        for (int id : genres) {
            strGenres.append(genreHelper.getGenreById(id).getName()).append(", ");
        }

        // Trim trailing ", " if any genres have been added
        if (strGenres.length() > 3) {
            strGenres = new StringBuilder(strGenres.substring(0, strGenres.length() - 2));
        }

        // Load the view with values
        tvTitle.setText(movie.getTitle());
        tvReleaseDate.setText(movie.getReleaseDate());
        tvAverageVotes.setText(String.format(Locale.getDefault(), "%.1f", movie.getVoteAverage()));
        tvSynopsis.setText(movie.getOverview());

        // If we have no genres there is
        // no point in displaying the header...
        if (strGenres.length() == 0) {
            TextView tvGenreLabel = findViewById(R.id.tv_genre_label);
            tvGenreLabel.setVisibility(View.INVISIBLE);
            tvGenres.setVisibility(View.INVISIBLE);
        } else {
            TextView tvGenreLabel = findViewById(R.id.tv_genre_label);
            tvGenreLabel.setVisibility(View.VISIBLE);
            tvGenres.setVisibility(View.VISIBLE);
            tvGenres.setText(strGenres.toString());
        }

        // Favorites image
        // @android:drawable/btn_star_big_off
        // @android:drawable/btn_star_big_on


    }

    @Override
    public void OnMovieListReady(MovieListModel movieList) {
        Log.d(TAG, "Called: OnMovieListReady");
    }

    @Override
    public void OnMovieListFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieListFailure");
    }

    @Override
    public void OnMovieListDataReady(List<MovieModel> movies) {
        Log.d(TAG, "Called: OnMovieListDataReady");
    }

    @Override
    public void OnMovieListDataFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieListDataFailure");
    }

    @Override
    public void OnMovieDetailsReady(Movie movie) {
        Log.d(TAG, "Called: OnMovieListDetailsReady");
    }

    @Override
    public void OnMovieDetailsFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieListDetailsFailure");
    }


    // Movie Trailers
    @Override
    public void OnMovieTrailersReady(List<MovieTrailerListModel> trailerList) {
        Log.d(TAG, "Called: OnMovieTrailersReady");
    }

    @Override
    public void OnMovieTrailerListFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieTrailerListReady");
        // Get Movie Reviews
        // Making this call here seems like a hack!
        // However, the only way to reliably get the trailer
        // images to show seems to be to serialize the calls
        // by daisy chaining them....
        mApiService.movieReviewsRequest(String.valueOf(mMovie.getId()));
    }

    @Override
    public void OnMovieTrailersDataReady(List<MovieTrailerModel> trailerList) {
        Log.d(TAG, "Called: OnMovieTrailerDataReady");
        if (null == trailerList) {
            String msg = getString(R.string.illegal_argument_exception);
            throw new IllegalArgumentException(msg);
        }
        mMovieTrailers = trailerList;
        YoutubeImageHelper trailerHelper = new YoutubeImageHelper(this);
        //trailerHelper.setDebugMode(true);
        trailerHelper.addListener(this);

        int count = 0;
        for (MovieTrailerModel trailer : mMovieTrailers) {
            trailerHelper.getDrawable(count, String.valueOf(trailer.key));
            count++;

            // Limit to only three trailers
            // Thought this would help the display issue.
            // It does but still doesnt give 100% correct results.
            // Some images still don't show on initial visit to
            // the detail page.
            if (count > 1) break;
        }

        // Force redraw of page. Hopefully this will
        // repaint the page os the images always show up.
        ViewGroup vg = findViewById(R.id.detail_activity_container);
        vg.invalidate();


        // Get Movie Reviews
        // Making this call here seems like a hack!
        // However, the only way to reliably get the trailer
        // images to show seems to be to serialize the calls
        // by daisy chaining them....
        mApiService.movieReviewsRequest(String.valueOf(mMovie.getId()));

    }

    @Override
    public void OnMovieTrailersDataFailure(Throwable t) {
        Log.d(TAG, "Called: OnMovieTrailerDataFailure");
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }


    private void insertTrailerRowItem(MovieTrailerModel trailer, Bitmap bitmap) {
        Log.d(TAG, "Called: insertTrailerRowItem");
        // Inflate a row view object
        View view = mInflater.inflate(R.layout.trailer_row, null, false);

        TextView tvTitle = view.findViewById(R.id.tv_trailer_title);
        tvTitle.setText(trailer.name);

        ImageButton btnPlay = view.findViewById(R.id.ib_trailer_play);
        btnPlay.setTag(trailer.key);
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String movieId = (String) v.getTag();
                String url = String.format(mTrailerUrl, movieId);

                Intent playerIntent = new Intent(Intent.ACTION_VIEW);

                playerIntent.setData(Uri.parse(url));
                startActivity(playerIntent);
            }
        });

        // Get the trailer image
        ImageView imgView = view.findViewById(R.id.img_trailer);
        imgView.setImageBitmap(bitmap);
        //imgView.setMinimumHeight(100);
        //imgView.setMinimumWidth(200);

        //mTrailerLayout.setMinimumHeight(300);
        mTrailerLayout.addView(view);

        // Force redraw of page. Hopefully this will
        // repaint the page os the images always show up.
        mTrailerLayout.invalidate();

    }

    @Override
    public void onYoutubeBitmapLoaded(int index, Bitmap bitmap, Picasso.LoadedFrom from) {
        insertTrailerRowItem(mMovieTrailers.get(index), bitmap);
        //notifyAll();
    }

    @Override
    public void onYoutubeBitmapFailed(int index, Exception e, Drawable errorDrawable) {
        Log.d(TAG, "Youtube Image loading failed: " + e.getMessage().toString());
    }

    @Override
    public void onYoutubePrepareLoad(int index, Drawable placeHolderDrawable) {

    }

    @Override
    public void OnMovieReviewsReady(List<MovieReviewListModel> reviewList) {
        notify();
    }

    @Override
    public void OnMovieReviewListFailure(Throwable t) {

    }

    @Override
    public void OnMovieReviewDataReady(List<MovieReviewModel> reviewList) {
        //mReviews = reviewList;
        //insertReviewRows(reviewList);
        // Get the linear layout used for the list
        LinearLayout reviewLayout = findViewById(R.id.ll_movie_review_list);

        // Add all reviews to the list
        for (MovieReviewModel review : reviewList) {

            // Inflate a row view object
            // Make sure root is null or this will overwrite the same object on each iteration!
            View view = mInflater.inflate(R.layout.review_row, null, false);

            TextView tvAuthor = view.findViewById(R.id.tv_review_author);
            tvAuthor.setText(review.author);

            TextView tvBody = view.findViewById(R.id.tv_review_body);
            tvBody.setText(review.content);

            // Now add the row to the linear layout
            reviewLayout.addView(view);//, mTrailerCount++);
        }

        // Now we invalidate the parent view to cause it to be redrawn.
        // Though this would allow all movie trailers to show up but it doesn't work 100%.
        ViewGroup vg = findViewById(R.id.detail_scrollview_container);
        vg.invalidate();
    }

    @Override
    public void OnMovieReviewDataFailure(Throwable t) {

    }




}
