package us.sensornet.popularmovieappv3.Database;

import android.provider.BaseColumns;

public final class FavoritesDbContract {

    // Unused Constructor
    private FavoritesDbContract() {
    }

    // Define the table contents
    public static class FavoritesEntry implements BaseColumns {
        public static final String TABLE_NAME = "favorites";
        public static final String TITLE = "title";
        public static final String SUMMARY = "summary";
        //public static final String
    }

}
