package us.sensornet.popularmovieappv3.Api;

import android.graphics.Movie;

import java.util.List;

import us.sensornet.popularmovieappv3.Model.MovieListModel;
import us.sensornet.popularmovieappv3.Model.MovieModel;
import us.sensornet.popularmovieappv3.Model.MovieReviewListModel;
import us.sensornet.popularmovieappv3.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3.Model.MovieTrailerListModel;
import us.sensornet.popularmovieappv3.Model.MovieTrailerModel;

// TODO -- Refactor this interface into multiple interfaces
// This interface is far to large. All the methods must be implemented in
// any class that requires even a small part of the interface.  For this reason
// it should be broken down into separate interfaces for movie lists, movie details
// movie trailers and movie reviews.
public interface ServiceInterface {
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListReady(MovieListModel movieList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListFailure(Throwable t);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListDataReady(List<MovieModel> movies);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieListDataFailure(Throwable t);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieDetailsReady(Movie movie);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieDetailsFailure(Throwable t);


    // Movie Trailers
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieTrailersReady(List<MovieTrailerListModel> trailerList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieTrailerListFailure(Throwable t);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieTrailersDataReady(List<MovieTrailerModel> trailerList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieTrailersDataFailure(Throwable t);

    // Movie Reviews
    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieReviewsReady(List<MovieReviewListModel> trailerList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieReviewListFailure(Throwable t);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieReviewDataReady(List<MovieReviewModel> trailerList);

    @SuppressWarnings({"EmptyMethod", "unused"})
    void OnMovieReviewDataFailure(Throwable t);
}
