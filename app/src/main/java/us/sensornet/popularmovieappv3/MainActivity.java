package us.sensornet.popularmovieappv3;

import android.content.Intent;
import android.graphics.Movie;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import butterknife.ButterKnife;
import us.sensornet.popularmovieappv3.Adapter.MovieAdapter;
import us.sensornet.popularmovieappv3.Api.Service;
import us.sensornet.popularmovieappv3.Api.ServiceInterface;
import us.sensornet.popularmovieappv3.Model.MovieListModel;
import us.sensornet.popularmovieappv3.Model.MovieModel;
import us.sensornet.popularmovieappv3.Model.MovieReviewListModel;
import us.sensornet.popularmovieappv3.Model.MovieReviewModel;
import us.sensornet.popularmovieappv3.Model.MovieTrailerListModel;
import us.sensornet.popularmovieappv3.Model.MovieTrailerModel;


public class MainActivity extends AppCompatActivity implements ServiceInterface, MovieAdapter.GridItemClickListener {
    @SuppressWarnings("SpellCheckingInspection")
    private static final String RECYCLER_LAYOUT_KEY = "recyclerview_layout";
    private static Bundle mLayoutBundle;
    @SuppressWarnings("unused")
    private final String TAG = getClass().getSimpleName();
    private final String SEARCH_MODE_KEY = "search_mode";
    //private final String GRID_VIEW_STATE_KEY = "gridState";
    private List<MovieModel> mMovies;
    private List<MovieTrailerModel> mTrailers;
    private Service mApiService;
    @SuppressWarnings("FieldCanBeLocal")
    private MovieAdapter mMovieAdapter;
    private String mMode = Service.QueryModes.MostPopular;
    private RecyclerView mRecyclerView;
    @SuppressWarnings("FieldCanBeLocal")
    private RecyclerView.LayoutManager mLayoutManager;
    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private Parcelable savedRecyclerLayoutState;
    

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity_recycler_view);
        ButterKnife.bind(this);

        mRecyclerView = findViewById(R.id.rv_main_container);
        mRecyclerView.setHasFixedSize(true);

        // use a Grid layout manager
        mLayoutManager = new GridLayoutManager(this, 2);
        mRecyclerView.setLayoutManager(mLayoutManager);

        // Note the MovieAdapter will have no data
        // until the Service returns from the Api call
        mMovieAdapter = new MovieAdapter(this, this);

        mApiService = new Service(this, false);
        // Add this activity as listener
        mApiService.addListener(this);
        // Call the apiService
        mApiService.popularMovieRequest(mMode);

    }

    // Save instance state
    @SuppressWarnings("EmptyMethod")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(RECYCLER_LAYOUT_KEY, mLayoutManager.onSaveInstanceState());
        outState.putString(SEARCH_MODE_KEY, mMode);
    }

    // Restore user experience
    @SuppressWarnings("EmptyMethod")
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //restore recycler view at same position
        if (savedInstanceState != null) {
            savedRecyclerLayoutState = savedInstanceState.getParcelable(RECYCLER_LAYOUT_KEY);
            mMode = savedInstanceState.getString(SEARCH_MODE_KEY, Service.QueryModes.MostPopular);
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (null != mLayoutBundle) {
            mLayoutManager.onRestoreInstanceState(mLayoutBundle.getParcelable(RECYCLER_LAYOUT_KEY));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (null == mLayoutBundle) {
            mLayoutBundle = new Bundle();
        }
        mLayoutBundle.putParcelable(RECYCLER_LAYOUT_KEY, mLayoutManager.onSaveInstanceState());
    }

    // Creates option menu items
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    // Handle option menu items
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mnu_item_top_rated:
                mMode = Service.QueryModes.TopRated;
                mApiService.popularMovieRequest(mMode);
                return true;

            case R.id.mnu_item_most_popular:
                mMode = Service.QueryModes.MostPopular;
                mApiService.popularMovieRequest(mMode);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Launches the Detail Activity
    private void showDetails(MovieModel movie) {
        if (null == movie) {
            String msg = getString(R.string.illegal_argument_exception);
            throw new IllegalArgumentException(msg);
        }

        Intent detailIntent = new Intent(this, DetailActivity.class);
        Gson gson = new Gson();
        String jsonMovie = gson.toJson(movie);
        // TODO pass movie trailers here
        detailIntent.putExtra("jsonMovie", jsonMovie);
        startActivity(detailIntent);
    }


    @Override
    public void onGridItemClick(View view) {
        mMovieAdapter.setMovieData(mMovies);
        int movieId = (int) view.getTag();
        showDetails(mMovieAdapter.getMovieById(movieId));
    }


    //===================================================
    // Methods below are callbacks for ServiceInterface
    // These methods are part of the api service
    // interface and must be provided for callback
    // even if you do not intend to use them.
    //===================================================
    @Override
    public void OnMovieListReady(MovieListModel movieList) {
        //mMovies = movieList.getResults();
        //mMovieAdapter = new MovieAdapter(this, mMovies, this);
    }

    @Override
    public void OnMovieListFailure(Throwable t) {
        String msg = getString(R.string.no_data_available_message);
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        Log.e(TAG, "Error: " + t.getMessage());
    }

    @Override
    public void OnMovieListDataReady(List<MovieModel> movies) {
        if (null == movies) {
            String msg = getString(R.string.illegal_argument_exception);
            throw new IllegalArgumentException(msg);
        }
        mMovies = movies;
        // We can't set the adapter until we have data from the api service
        mMovieAdapter.setMovieData(movies);
        mRecyclerView.setAdapter(mMovieAdapter);
    }

    @Override
    public void OnMovieListDataFailure(Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void OnMovieDetailsReady(Movie movie) {

    }

    @Override
    public void OnMovieDetailsFailure(Throwable t) {
        Toast.makeText(this, t.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void OnMovieTrailersReady(List<MovieTrailerListModel> trailerList) {

    }

    @Override
    public void OnMovieTrailerListFailure(Throwable t) {

    }

    @Override
    public void OnMovieTrailersDataReady(List<MovieTrailerModel> trailerList) {

    }

    @Override
    public void OnMovieTrailersDataFailure(Throwable t) {

    }

    @Override
    public void OnMovieReviewsReady(List<MovieReviewListModel> trailerList) {

    }

    @Override
    public void OnMovieReviewListFailure(Throwable t) {

    }

    @Override
    public void OnMovieReviewDataReady(List<MovieReviewModel> trailerList) {

    }

    @Override
    public void OnMovieReviewDataFailure(Throwable t) {

    }
}
